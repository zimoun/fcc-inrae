#!/usr/bin/env bash


guix time-machine -C channels.scm \
     -- shell -m manifest.scm     \
     -- rubber --pdf pres.tex
